#!/bin/sh
if [ "$1" == "-h" -o "$1" == "--help" ] 
then
  echo "Usage: ./hplip-rebase.sh X.Y.Z"
  echo "       X.Y.Z - new hplip version"
  exit 0
fi

wget -O hplip-plugin.run http://www.openprinting.org/download/printdriver/auxfiles/HP/plugins/hplip-$1-plugin.run

if [ $? -gt 0 ]
then
  echo "Error while downloading plugin. Check version passed by argument or internet connection. For usage run script with -h or --help argument."
  rm hplip-plugin.run
  exit 1
fi
rm hplip-plugin.run

fedpkg mockbuild
if [ $? -gt 0 ]
then
  echo "Error during mockbuild."
  exit 1
fi
exit 0
